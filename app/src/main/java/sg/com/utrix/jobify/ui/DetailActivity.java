package sg.com.utrix.jobify.ui;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.provider.JobContract;
import sg.com.utrix.jobify.ui.widget.CheckableFrameLayout;
import sg.com.utrix.jobify.ui.widget.ObservableScrollView;
import sg.com.utrix.jobify.util.Config;
import sg.com.utrix.jobify.util.LPreviewUtilsBase;
import sg.com.utrix.jobify.util.UIUtils;

import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;
import static sg.com.utrix.jobify.util.TimeUtils.getTimeAgo;
import static sg.com.utrix.jobify.util.TimeUtils.timestampToMillis;


public class DetailActivity extends BaseActivity implements
        LoaderManager.LoaderCallbacks<Cursor>, ObservableScrollView.Callbacks {

    private static final String TAG = makeLogTag(DetailActivity.class);
    private static final float GAP_FILL_DISTANCE_MULTIPLIER = 1.5f;
    private float mMaxHeaderElevation;
    private float mFABElevation;
    private boolean mHasPhoto = true;
    private String mId;
    private String mSource;
    boolean mGapFillShown;

    private TextView mJobTitle;
    private TextView mCompanyTitle;
    private TextView mJobDescription;
    private TextView mCompanyAddress;
    private Activity mRootView;

    private String mObjectIdValue;
    private String mPostDateValue;
    private String mClosingDateValue;
    private String mJobTitleValue;
    private String mJobDescriptionValue;
    private String mJobLatValue;
    private String mJobLongValue;
    private String mCompanyNameValue;
    private String mCompanyAddressValue;
    private String mCompanyLogoValue;
    private String mCompanyContactValue;
    private String mMaxSalaryValue;
    private String mMinSalaryValue;
    private String mExperienceValue;
    private String mSourceValue;
    private String mJobUrlValue;


    private GoogleMap mMap;
    private ObservableScrollView mScrollView;
    private CheckableFrameLayout addBookmarkButton;
    private int mHeaderTopClearance;
    private int mPhotoHeightPixels;
    private int mHeaderHeightPixels;
    private int mAddScheduleButtonHeightPixels;

    private View mHeaderContentBox;
    private View mPhotoViewContainer;
    private View mHeaderBackgroundBox;
    private View mDetailsContainer;

    private View mHeaderBox;
    private View mHeaderShadow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        autoShowOrHideActionBar(false);

        setUpMapIfNeeded();

        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        mRootView = this;

        findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                mRootView.overridePendingTransition(R.anim.stay,R.anim.slide_out_bottom);
            }
        });

        Bundle extras = getIntent().getExtras();
        mId = extras.getString(Config.JOB_ID_BUNDLE);
        mSource = extras.getString(Config.JOB_BUNDLE_SOURCE);

        this.findViewById(R.id.apply_now_btn).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, WebActivity.class);
                intent.putExtra("JOB_URL", mJobUrlValue);
                startActivity(intent);

            }
        });




        mHeaderContentBox = mRootView.findViewById(R.id.header_session_contents);
        mPhotoViewContainer = mRootView.findViewById(R.id.session_photo_container);
        mHeaderBackgroundBox = mRootView.findViewById(R.id.header_background);
        mDetailsContainer = mRootView.findViewById(R.id.details_container);
        mHeaderBox = mRootView.findViewById(R.id.header_session);
        mHeaderShadow = mRootView.findViewById(R.id.header_shadow);
        addBookmarkButton = (CheckableFrameLayout)mRootView.findViewById(R.id.add_schedule_button);
        mJobTitle = (TextView) mRootView.findViewById(R.id.job_title_text);
        mCompanyTitle = (TextView) mRootView.findViewById(R.id.company_name_text);
        mJobDescription = (TextView) mRootView.findViewById(R.id.job_description_text);
        mCompanyAddress = (TextView) mRootView.findViewById(R.id.company_address_text);

        int mSessionColor = getResources().getColor(R.color.default_session_color);
        mHeaderBackgroundBox.setBackgroundColor(mSessionColor);
        mMaxHeaderElevation = getResources().getDimensionPixelSize(R.dimen.session_detail_max_header_elevation);
        mFABElevation = getResources().getDimensionPixelSize(R.dimen.fab_elevation);

        setupCustomScrolling(this);

        if (mSource.equals("listing")) {
            addBookmarkButton.setVisibility(View.VISIBLE);
        }
        else {
            addBookmarkButton.setVisibility(View.GONE);
        }


        getLoaderManager().initLoader(0, null, this);


        /* Set onClick Listener */
        addBookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ContentValues contentValues = new ContentValues();
                contentValues.put(JobContract.BookmarkJobs.OBJECTID, mId );
                contentValues.put(JobContract.BookmarkJobs.URL, mJobUrlValue );
                contentValues.put(JobContract.BookmarkJobs.POST_DATE, mPostDateValue );
                contentValues.put(JobContract.BookmarkJobs.CLOSING_DATE, mClosingDateValue );
                contentValues.put(JobContract.BookmarkJobs.TITLE, mJobTitleValue );
                contentValues.put(JobContract.BookmarkJobs.DESCRIPTION,mJobDescriptionValue );
                contentValues.put(JobContract.BookmarkJobs.LAT,mJobLatValue );
                contentValues.put(JobContract.BookmarkJobs.LONG,mJobLongValue );
                contentValues.put(JobContract.BookmarkJobs.COMPANY_NAME,mCompanyNameValue );
                contentValues.put(JobContract.BookmarkJobs.COMPANY_ADDRESS, mCompanyAddressValue );
                contentValues.put(JobContract.BookmarkJobs.COMPANY_LOGO_URL,mCompanyLogoValue );
                contentValues.put(JobContract.BookmarkJobs.CONTACT,mCompanyContactValue );
                contentValues.put(JobContract.BookmarkJobs.MAX_SALARY,mMaxSalaryValue );
                contentValues.put(JobContract.BookmarkJobs.MIN_SALARY,mMinSalaryValue );
                contentValues.put(JobContract.BookmarkJobs.EXPERIENCE,mExperienceValue );
                contentValues.put(JobContract.BookmarkJobs.SOURCE,mSourceValue );


                AsyncQueryHandler handler = new AsyncQueryHandler(mRootView.getContentResolver()) {};

                Uri addBookmarkUri = JobContract.BookmarkJobs.CONTENT_URI
                        .buildUpon()
                        .appendPath("insert")
                        .build();

                handler.startInsert(-1, null, addBookmarkUri, contentValues);

                Toast.makeText(mRootView, mJobTitle.getText().toString() + " added to your favourites",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupCustomScrolling(Activity rootView) {
        mScrollView = (ObservableScrollView) rootView.findViewById(R.id.scroll_view);

        mScrollView.addCallbacks(this);
        ViewTreeObserver vto = mScrollView.getViewTreeObserver();
        if (vto.isAlive()) {
            vto.addOnGlobalLayoutListener(mGlobalLayoutListener);
        }
    }

    private ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener
            = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            mAddScheduleButtonHeightPixels = addBookmarkButton.getHeight();
            recomputePhotoAndScrollingMetrics();
        }
    };

    private void recomputePhotoAndScrollingMetrics() {

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height=dm.heightPixels;

        final int actionBarSize = UIUtils.calculateActionBarSize(this);
        mHeaderTopClearance = actionBarSize - mHeaderContentBox.getPaddingTop();
        mHeaderHeightPixels = mHeaderContentBox.getHeight();

        mPhotoHeightPixels = mHeaderTopClearance;
        if (mHasPhoto) {
            mPhotoHeightPixels = 500;
            mPhotoHeightPixels = Math.min(mPhotoHeightPixels, height * 1 / 3);
        }

        ViewGroup.LayoutParams lp;
        lp = mPhotoViewContainer.getLayoutParams();
        if (lp.height != mPhotoHeightPixels) {
            lp.height = mPhotoHeightPixels;
            mPhotoViewContainer.setLayoutParams(lp);
        }

        lp = mHeaderBackgroundBox.getLayoutParams();
        if (lp.height != mHeaderHeightPixels) {
            lp.height = mHeaderHeightPixels;
            mHeaderBackgroundBox.setLayoutParams(lp);
        }

        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams)
                mDetailsContainer.getLayoutParams();
        if (mlp.topMargin != mHeaderHeightPixels + mPhotoHeightPixels) {
            mlp.topMargin = mHeaderHeightPixels + mPhotoHeightPixels;
            mDetailsContainer.setLayoutParams(mlp);
        }

        onScrollChanged(0, 0); // trigger scroll handling
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                // The Map is verified. It is now safe to manipulate the map.

            }
        }
    }

    @Override
    public void onScrollChanged(int deltaX, int deltaY) {
        final BaseActivity activity = DetailActivity.this;
        if (activity == null) {
            return;
        }

        // Reposition the header bar -- it's normally anchored to the top of the content,
        // but locks to the top of the screen on scroll
        int scrollY = mScrollView.getScrollY();

        float newTop = Math.max(mPhotoHeightPixels, scrollY + mHeaderTopClearance);
        mHeaderBox.setTranslationY(newTop);
        addBookmarkButton.setTranslationY(newTop + mHeaderHeightPixels
                - mAddScheduleButtonHeightPixels / 2);

        mHeaderBackgroundBox.setPivotY(mHeaderHeightPixels);
        int gapFillDistance = (int) (mHeaderTopClearance * GAP_FILL_DISTANCE_MULTIPLIER);
        boolean showGapFill = !mHasPhoto || (scrollY > (mPhotoHeightPixels - gapFillDistance));
        float desiredHeaderScaleY = showGapFill ?
                ((mHeaderHeightPixels + gapFillDistance + 1) * 1f / mHeaderHeightPixels)
                : 1f;
        if (!mHasPhoto) {
            mHeaderBackgroundBox.setScaleY(desiredHeaderScaleY);
        } else if (mGapFillShown != showGapFill) {
            mHeaderBackgroundBox.animate()
                    .scaleY(desiredHeaderScaleY)
                    .setInterpolator(new DecelerateInterpolator(2f))
                    .setDuration(250)
                    .start();
        }
        mGapFillShown = showGapFill;

        LPreviewUtilsBase lpu = activity.getLPreviewUtils();

        mHeaderShadow.setVisibility(lpu.hasLPreviewAPIs() ? View.GONE : View.VISIBLE);

        if (mHeaderTopClearance != 0) {
            // Fill the gap between status bar and header bar with color
            float gapFillProgress = Math.min(Math.max(UIUtils.getProgress(scrollY,
                    mPhotoHeightPixels - mHeaderTopClearance * 2,
                    mPhotoHeightPixels - mHeaderTopClearance), 0), 1);
            lpu.setViewElevation(mHeaderBackgroundBox, gapFillProgress * mMaxHeaderElevation);
            lpu.setViewElevation(mHeaderContentBox, gapFillProgress * mMaxHeaderElevation + 0.1f);
            lpu.setViewElevation(addBookmarkButton, gapFillProgress * mMaxHeaderElevation + mFABElevation);
            if (!lpu.hasLPreviewAPIs()) {
                mHeaderShadow.setAlpha(gapFillProgress);
            }
        }

        // Move background photo (parallax effect)
        mPhotoViewContainer.setTranslationY(scrollY * 0.5f);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri uri = null;


        if (mSource.equals("listing")) {

            uri = JobContract.Jobs
                    .CONTENT_URI.buildUpon()
                    .appendPath("id")
                    .appendPath(String.valueOf(mId))
                    .build();
        }

        if (mSource.equals("favourite")) {

            uri = JobContract.BookmarkJobs
                    .CONTENT_URI.buildUpon()
                    .appendPath("id")
                    .appendPath(String.valueOf(mId))
                    .build();
        }


        return new CursorLoader(this, uri, null, null, null, null);
    }




    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        data.moveToFirst();
        String jobDescription;

        com.commonsware.cwac.anddown.AndDown andDown = new com.commonsware.cwac.anddown.AndDown();

        //Assign Db value to string
        mObjectIdValue=data.getString(JobQuery.ID);
        mJobUrlValue=data.getString(JobQuery.JOB_URL);
        mPostDateValue=data.getString(JobQuery.POST_DATE);
        mClosingDateValue=data.getString(JobQuery.CLOSING_DATE);
        mJobTitleValue=data.getString(JobQuery.TITLE);
        mJobDescriptionValue= andDown.markdownToHtml(data.getString(JobQuery.DESCRIPTION));
        mJobLatValue=data.getString(JobQuery.LAT);
        mJobLongValue=data.getString(JobQuery.LONG);
        mCompanyNameValue=data.getString(JobQuery.COMPANY_NAME);
        mCompanyAddressValue=data.getString(JobQuery.COMPANY_ADDRESS);
        mCompanyLogoValue=data.getString(JobQuery.COMPANY_LOGO_URL);
        mCompanyContactValue=data.getString(JobQuery.CONTACT);
        mMaxSalaryValue=data.getString(JobQuery.MAX_SALARY);
        mMinSalaryValue=data.getString(JobQuery.MIN_SALARY);
        mExperienceValue=data.getString(JobQuery.EXPERIENCE);
        mSourceValue=data.getString(JobQuery.SOURCE);
        mJobUrlValue = data.getString(JobQuery.JOB_URL);

        mJobTitle.setText(mJobTitleValue);
        mCompanyTitle.setText(mCompanyNameValue);
        mCompanyAddress.setText(mCompanyAddressValue);

        long time = timestampToMillis(mPostDateValue, System.currentTimeMillis());
        String timeAgo = getTimeAgo(time, this);

        jobDescription = String.format("%s <br/><br/> posted %s" ,
                mJobDescriptionValue,
                timeAgo
                );

        mJobDescription.setText(Html.fromHtml(jobDescription) );
        mJobDescription.setMovementMethod(LinkMovementMethod.getInstance());

        LatLng location = new LatLng(data.getFloat(JobQuery.LAT), data.getFloat(JobQuery.LONG));

        mMap.addMarker(new MarkerOptions()
                .position(location)
                .title(data.getString(JobQuery.COMPANY_NAME)));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);





    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }




    private interface JobQuery {

        int ID = 0;
        int JOB_URL = 2;
        int POST_DATE = 3;
        int CLOSING_DATE = 4;
        int TITLE = 5;
        int DESCRIPTION = 6;
        int LAT = 7;
        int LONG = 8;
        int COMPANY_NAME = 9;
        int COMPANY_ADDRESS = 10;
        int COMPANY_LOGO_URL = 11;
        int CONTACT = 12;
        int MAX_SALARY = 13;
        int MIN_SALARY = 14;
        int EXPERIENCE = 15;
        int SOURCE = 16;
    }
}
