/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sg.com.utrix.jobify.ui;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.util.LPreviewUtils;
import sg.com.utrix.jobify.util.LPreviewUtilsBase;

import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * A base activity that handles common functionality in the app. This includes the
 * navigation drawer, login and authentication, Action Bar tweaks, amongst others.
 */
public abstract class BaseActivity extends Activity
 {
    private static final String TAG = makeLogTag(BaseActivity.class);


    // Navigation drawer:
    private DrawerLayout mDrawerLayout;
    private LPreviewUtilsBase.ActionBarDrawerToggleWrapper mDrawerToggle;

    // allows access to L-Preview APIs through an abstract interface so we can compile with
    // both the L Preview SDK and with the API 19 SDK
    private LPreviewUtilsBase mLPreviewUtils;

    private ObjectAnimator mStatusBarColorAnimator;
    private LinearLayout mAccountListContainer;
    private ViewGroup mDrawerItemsListContainer;
    private Handler mHandler;

    private ImageView mExpandAccountBoxIndicator;
    private boolean mAccountBoxExpanded = false;

    // When set, these components will be shown/hidden in sync with the action bar
    // to implement the "quick recall" effect (the Action Bar and the header views disappear
    // when you scroll down a list, and reappear quickly when you scroll up).
    private ArrayList<View> mHideableHeaderViews = new ArrayList<View>();

    // Durations for certain animations we use:
    private static final int HEADER_HIDE_ANIM_DURATION = 300;
    private static final int ACCOUNT_BOX_EXPAND_ANIM_DURATION = 200;


    // delay to launch nav drawer item, to allow close animation to play
    private static final int NAVDRAWER_LAUNCH_DELAY = 250;

    // fade in and fade out durations for the main content when switching between
    // different Activities of the app through the Nav Drawer
    private static final int MAIN_CONTENT_FADEOUT_DURATION = 150;
    private static final int MAIN_CONTENT_FADEIN_DURATION = 250;

    // list of navdrawer items that were actually added to the navdrawer, in order
    private ArrayList<Integer> mNavDrawerItems = new ArrayList<Integer>();

    // views that correspond to each navdrawer item, null if not yet created
    private View[] mNavDrawerItemViews = null;


    // asynctask that performs GCM registration in the backgorund
    private AsyncTask<Void, Void, Void> mGCMRegisterTask;

    // handle to our sync observer (that notifies us about changes in our sync state)
    private Object mSyncObserverHandle;

    // data bootstrap thread. Data bootstrap is the process of initializing the database
    // with the data cache that ships with the app.
    Thread mDataBootstrapThread = null;

    // variables that control the Action Bar auto hide behavior (aka "quick recall")
    private boolean mActionBarAutoHideEnabled = false;
    private int mActionBarAutoHideSensivity = 0;
    private int mActionBarAutoHideMinY = 0;
    private int mActionBarAutoHideSignal = 0;
    private boolean mActionBarShown = true;

    // A Runnable that we should execute when the navigation drawer finishes its closing animation
    private Runnable mDeferredOnDrawerClosedRunnable;

    private boolean mManualSyncRequest;

    private int mThemedStatusBarColor;
    private int mProgressBarTopWhenActionBarShown;
    private static final TypeEvaluator ARGB_EVALUATOR = new ArgbEvaluator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ActionBar ab = getActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        mLPreviewUtils = LPreviewUtils.getInstance(this);
        mThemedStatusBarColor = getResources().getColor(R.color.theme_primary_dark);
    }





     public LPreviewUtilsBase getLPreviewUtils() {
         return mLPreviewUtils;
     }

     private void updateStatusBarForNavDrawerSlide(float slideOffset) {
         if (mStatusBarColorAnimator != null) {
             mStatusBarColorAnimator.cancel();
         }

         if (!mActionBarShown) {
             mLPreviewUtils.setStatusBarColor(Color.BLACK);
             return;
         }

         mLPreviewUtils.setStatusBarColor((Integer) ARGB_EVALUATOR.evaluate(slideOffset,
                 mThemedStatusBarColor, Color.BLACK));
     }

     private void initActionBarAutoHide() {
         mActionBarAutoHideEnabled = true;
         mActionBarAutoHideMinY = getResources().getDimensionPixelSize(
                 R.dimen.action_bar_auto_hide_min_y);
         mActionBarAutoHideSensivity = getResources().getDimensionPixelSize(
                 R.dimen.action_bar_auto_hide_sensivity);
     }

     protected void enableActionBarAutoHide(final ListView listView) {
         initActionBarAutoHide();
         listView.setOnScrollListener(new AbsListView.OnScrollListener() {
             final static int ITEMS_THRESHOLD = 3;
             int lastFvi = 0;

             @Override
             public void onScrollStateChanged(AbsListView view, int scrollState) {
             }

             @Override
             public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                 onMainContentScrolled(firstVisibleItem <= ITEMS_THRESHOLD ? 0 : Integer.MAX_VALUE,
                         lastFvi - firstVisibleItem > 0 ? Integer.MIN_VALUE :
                                 lastFvi == firstVisibleItem ? 0 : Integer.MAX_VALUE
                 );
                 lastFvi = firstVisibleItem;
             }
         });
     }

     /**
      * Indicates that the main content has scrolled (for the purposes of showing/hiding
      * the action bar for the "action bar auto hide" effect). currentY and deltaY may be exact
      * (if the underlying view supports it) or may be approximate indications:
      * deltaY may be INT_MAX to mean "scrolled forward indeterminately" and INT_MIN to mean
      * "scrolled backward indeterminately".  currentY may be 0 to mean "somewhere close to the
      * start of the list" and INT_MAX to mean "we don't know, but not at the start of the list"
      */
     private void onMainContentScrolled(int currentY, int deltaY) {
         if (deltaY > mActionBarAutoHideSensivity) {
             deltaY = mActionBarAutoHideSensivity;
         } else if (deltaY < -mActionBarAutoHideSensivity) {
             deltaY = -mActionBarAutoHideSensivity;
         }

         if (Math.signum(deltaY) * Math.signum(mActionBarAutoHideSignal) < 0) {
             // deltaY is a motion opposite to the accumulated signal, so reset signal
             mActionBarAutoHideSignal = deltaY;
         } else {
             // add to accumulated signal
             mActionBarAutoHideSignal += deltaY;
         }

         boolean shouldShow = currentY < mActionBarAutoHideMinY ||
                 (mActionBarAutoHideSignal <= -mActionBarAutoHideSensivity);
         autoShowOrHideActionBar(shouldShow);
     }

     protected void autoShowOrHideActionBar(boolean show) {
         if (show == mActionBarShown) {
             return;
         }

         mActionBarShown = show;
         getLPreviewUtils().showHideActionBarIfPartOfDecor(show);
         onActionBarAutoShowOrHide(show);
     }

     protected void onActionBarAutoShowOrHide(boolean shown) {
         if (mStatusBarColorAnimator != null) {
             mStatusBarColorAnimator.cancel();
         }
         mStatusBarColorAnimator = ObjectAnimator.ofInt(mLPreviewUtils, "statusBarColor",
                 shown ? mThemedStatusBarColor : Color.BLACK).setDuration(250);
         mStatusBarColorAnimator.setEvaluator(ARGB_EVALUATOR);
         mStatusBarColorAnimator.start();


         for (View view : mHideableHeaderViews) {
             if (shown) {
                 view.animate()
                         .translationY(0)
                         .alpha(1)
                         .setDuration(HEADER_HIDE_ANIM_DURATION)
                         .setInterpolator(new DecelerateInterpolator());
             } else {
                 view.animate()
                         .translationY(-view.getBottom())
                         .alpha(0)
                         .setDuration(HEADER_HIDE_ANIM_DURATION)
                         .setInterpolator(new DecelerateInterpolator());
             }
         }
     }
}
