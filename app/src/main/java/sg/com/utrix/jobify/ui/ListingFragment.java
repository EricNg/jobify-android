package sg.com.utrix.jobify.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardCursorAdapter;
import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.provider.JobContract;
import sg.com.utrix.jobify.provider.JobProvider;
import sg.com.utrix.jobify.util.Config;
import sg.com.utrix.jobify.util.ImageLoader;
import sg.com.utrix.jobify.util.PrefUtils;
import sg.com.utrix.jobify.util.UIUtils;

import static sg.com.utrix.jobify.util.LogUtils.LOGI;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;
import static sg.com.utrix.jobify.util.TimeUtils.getTimeAgo;
import static sg.com.utrix.jobify.util.TimeUtils.timestampToMillis;

import com.etiennelawlor.quickreturn.library.enums.QuickReturnType;
import com.etiennelawlor.quickreturn.library.listeners.QuickReturnListViewOnScrollListener;



/**
 * Created by HUANG on 19/7/2014.
 */
public class ListingFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = makeLogTag(ListingFragment.class);
    private final int mTagJob = 0;
    private final int mTagSearch = 1;

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private View mRootView;
    public static ListView mListView;
    private ListView mSearchHistoryList;
    private ListJobsAdapter mAdapter;
    private SimpleCursorAdapter mSearchAdapter;
    private ImageLoader mImageLoader;
    private View mSearchViewContainer;
    private EditText mSearchText;
    private LoaderManager.LoaderCallbacks mCallbacks;
    public static Handler mHandler = new Handler();
    public static View mLoaderView;
    public static SwipeRefreshLayout mSwipeRefreshLayout;


    public static Fragment newInstance(int sectionNumber) {
        ListingFragment fragment = new ListingFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mListView.setOnScrollListener(new QuickReturnListViewOnScrollListener
                (QuickReturnType.HEADER, mSearchViewContainer, -400, null, 0){

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                LOGI(TAG, "scroll");
                getLoaderManager().restartLoader(mTagJob, null, mCallbacks);
            }
        });

        mSwipeRefreshLayout.setProgressViewEndTarget(false,450);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLoaderManager().restartLoader(mTagJob, null, mCallbacks);
                //mListView.addHeaderView(mLoaderView);

                mSwipeRefreshLayout.setRefreshing(true);
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        mRootView = inflater.inflate(R.layout.fragment_listing, container, false);

        mSearchViewContainer = mRootView.findViewById(R.id.session_search_container);
        mSearchText = (EditText) mRootView.findViewById(R.id.search_text);
        mListView = (ListView) mRootView.findViewById(android.R.id.list);
        mSearchHistoryList = (ListView) mRootView.findViewById(R.id.search_history_list);
        mLoaderView = inflater.inflate(R.layout.loader_img, null, false);
        mCallbacks = this;

        mSearchText.setText(PrefUtils.getSearchText(getActivity(), PrefUtils.PREF_SEARCH_TEXT));

        mRootView.findViewById(R.id.drawer_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).openDrawer();
            }
            }
        );

        //Setup ImageLoader
        if (getActivity() instanceof ImageLoader.ImageLoaderProvider) {
            mImageLoader = ((ImageLoader.ImageLoaderProvider) getActivity()).getImageLoaderInstance();
        }


        mSwipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light);



        mSearchText.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mListView.setVisibility(View.GONE);
                mSearchHistoryList.setVisibility(View.VISIBLE);
                getLoaderManager().restartLoader(mTagSearch, null, mCallbacks);
                return false;
            }
        });

        mSearchText.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    //check if the right key was pressed
                    if (keyCode == KeyEvent.KEYCODE_BACK)
                    {
                        mSearchHistoryList.setVisibility(View.GONE);
                        mListView.setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });




        //Setup Search Text
        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    //Hide textBox
                    InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);

                    PrefUtils.setSearchText(getActivity(), mSearchText.getText().toString());

                    if(!searchEmpty()) {
                        final ContentValues contentValues = new ContentValues();
                        contentValues.put(JobContract.SearchHistory.SEARCH_TEXT,  mSearchText.getText().toString());
                        contentValues.put(JobContract.SearchHistory.SEARCH_DATE, System.currentTimeMillis());

                        AsyncQueryHandler handler = new AsyncQueryHandler(getActivity().getContentResolver()) {};

                        Uri addBookmarkUri = JobContract.SearchHistory.CONTENT_URI
                                .buildUpon()
                                .appendPath("insert")
                                .build();

                        handler.startInsert(-1, null, addBookmarkUri, contentValues);
                    }

                    JobProvider.clearDbflag = true;
                    getLoaderManager().restartLoader(mTagJob, null, mCallbacks);
                    return true;
                }
                return false;
            }
        });

        mSearchAdapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_1, null,
                new String[] {JobContract.SearchHistory.SEARCH_TEXT},
                new int[] { android.R.id.text1}, 0);

        mSearchHistoryList.setAdapter(mSearchAdapter);

        mSearchHistoryList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {

                //Hide textBox
                InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
                mSearchText.setText(((TextView)view).getText().toString());
                PrefUtils.setSearchText(getActivity(), mSearchText.getText().toString());
                JobProvider.clearDbflag = true;
                getLoaderManager().restartLoader(mTagJob, null, mCallbacks);

            }
        });


        mListView.addFooterView(mLoaderView);

        //Set up Adapter
        mAdapter = new ListJobsAdapter(getActivity());
        if (mListView != null) {
            mListView.setAdapter(mAdapter);

        }

        getLoaderManager().initLoader(mTagJob, null, this);
        HideActionBar();

        return mRootView;
    }

    public void HideActionBar(){
        ((HomeActivity) getActivity()).autoShowOrHideActionBar(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

      LOGI(TAG, "onCreateLoader");
      Uri uri = null;
       switch (id) {
           case mTagJob:
               mLoaderView.setVisibility(View.VISIBLE);
               mListView.setVisibility(View.VISIBLE);
               mSearchHistoryList.setVisibility(View.GONE);

               uri = JobContract.Jobs.CONTENT_URI.buildUpon().build();
               if (!searchEmpty()) {
                   String queryString = "?keywords=" + Uri.encode(mSearchText.getText().toString() );
                   uri = Uri.parse(uri + queryString);
               }
               break;
           case mTagSearch:
               uri = JobContract.SearchHistory.CONTENT_URI.buildUpon().build();
               break;
           default:

       }

        //Flag to indicate call is from UI
        JobProvider.flag = true;

        return new CursorLoader(getActivity(), uri, null, null, null, null);
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        switch (loader.getId()) {
            case mTagJob:
                mAdapter.swapCursor(data);
                break;
            case mTagSearch:
                mSearchAdapter.swapCursor(data);
                break;

        }


    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    /*Private Functions*/
    private boolean searchEmpty() {
        Editable text = mSearchText.getText();
        if (text != null) {
            String textString = text.toString();
            return "".equals(textString);
        }
        return true;
    }


    private interface JobQuery {

        int ID = 0;
        int TITLE = 5;
        int POST_DATE = 3;
        int COMPANY_NAME = 9;
        int COMPANY_ADDRESS = 10;
        int COMPANY_LOGO_URL = 11;
        int SOURCE = 16;
    }

    private class ListJobsAdapter extends CardCursorAdapter {

        public ListJobsAdapter(Context context) {
            super(context);
        }

        @Override
        protected Card getCardFromCursor(Cursor cursor) {
            MyCursorCard cursorCard = new MyCursorCard(super.getContext());

            cursorCard.id = cursor.getString(JobQuery.ID);
            cursorCard.setId(cursor.getString(JobQuery.ID));
            cursorCard.title = cursor.getString(JobQuery.TITLE);
            cursorCard.postDate = cursor.getString(JobQuery.POST_DATE);
            cursorCard.companyName = cursor.getString(JobQuery.COMPANY_NAME);
            cursorCard.companyAddress = cursor.getString(JobQuery.COMPANY_ADDRESS);
            cursorCard.companyLogoUrl = cursor.getString(JobQuery.COMPANY_LOGO_URL);
            cursorCard.source = cursor.getString(JobQuery.SOURCE);

            //Simple clickListener
            cursorCard.setOnClickListener(new Card.OnCardClickListener() {
                @Override
                public void onClick(Card card, View view) {
                    //Toast.makeText(getContext(), "Card  id=" + id, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), DetailActivity.class);
                    intent.putExtra(Config.JOB_ID_BUNDLE, card.getId());
                    intent.putExtra(Config.JOB_BUNDLE_SOURCE, "listing" );
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in_bottom, R.anim.stay);
                }
            });

            return cursorCard;

        }

        private class MyCursorCard extends Card {

            String id;
            String title;
            String postDate;
            String companyName;
            String companyAddress;
            String companyLogoUrl;
            String source;


            public MyCursorCard(Context context) {

                super(context, R.layout.fragment_listing_item);
            }

            @Override
            public void setupInnerViewElements(ViewGroup parent, View view) {

                final ImageView imageView = (ImageView) view.findViewById(R.id.company_logo_image);

                if (mImageLoader != null) {
                    mImageLoader.get(companyLogoUrl, imageView);
                }

                TextView jobTitle = (TextView) view.findViewById(R.id.job_title_text);
                jobTitle.setTypeface(UIUtils.getHeaderFontThin(mContext));
                jobTitle.setText(title);

                ((TextView) view.findViewById(R.id.company_name_text)).setText(companyName);

                try {
                    ((TextView) view.findViewById(R.id.company_address_text)).setText(companyAddress.split("\n")[0]);
                } catch (Exception e){
                    //Do nothing for now
                }

                long time = timestampToMillis(postDate, System.currentTimeMillis());
                String timeAgo = getTimeAgo(time, getContext());

                ((TextView) view.findViewById(R.id.source_text)).setText(source + " - " + timeAgo);


            }
        }
    }


}