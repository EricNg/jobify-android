package sg.com.utrix.jobify.ui;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.util.ImageLoader;

import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;


public class HomeActivity extends BaseActivity
        implements
        NavigationDrawerFragment.NavigationDrawerCallbacks,
        ImageLoader.ImageLoaderProvider{

    private static final String TAG = makeLogTag(HomeActivity.class);
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    public DrawerLayout drawerLayout;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        autoShowOrHideActionBar(false);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        drawerLayout = (DrawerLayout) this.findViewById(R.id.drawer_layout);

        mImageLoader = new ImageLoader(this, R.drawable.person_image_empty).setFadeInImage(true);



    }

    public void openDrawer(){
        drawerLayout.openDrawer(Gravity.START);
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = new Fragment();
        switch (position){
            case 0:
                fragment = ListingFragment.newInstance(position);
                break;

            case 1:
                fragment = ListingFragment.newInstance(position);
                break;

            case 2:
                fragment = FavouriteFragment.newInstance(position);
                break;

            case 3:
                fragment = TimeLineFragment.newInstance(position);
                break;

            case 4:
                fragment = LoginFragment.newInstance(position);
                break;


        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();

    }

    public void onSectionAttached(int number) {
        String[] navItems = getResources().getStringArray(R.array.nav_drawer_items);

        switch (number) {
            case 1:
                mTitle = navItems[0];
                break;
            case 2:
                mTitle = navItems[1];
                break;
            case 3:
                mTitle = navItems[2];
                break;
            case 4:
                mTitle = navItems[3];
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.home, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public ImageLoader getImageLoaderInstance() {
        return mImageLoader;
    }

}
