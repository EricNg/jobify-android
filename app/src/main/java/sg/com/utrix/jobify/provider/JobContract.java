package sg.com.utrix.jobify.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract class for interacting with {@link JobDatabase}. Unless
 * otherwise noted, all time-based fields are milliseconds since epoch and can
 * be compared against {@link System#currentTimeMillis()}.
 * <p>
 * The backing {@link android.content.ContentProvider} assumes that {@link android.net.Uri}
 * are generated using stronger {@link String} identifiers, instead of
 * {@code int} {@link android.provider.BaseColumns#_ID} values, which are prone to shuffle during
 * sync.
 */
public class JobContract {

    /**
     * Special value for {@link SyncColumns#UPDATED} indicating that an entry
     * has never been updated, or doesn't exist yet.
     */
    public static final long UPDATED_NEVER = -2;

    /**
     * Special value for {@link SyncColumns#UPDATED} indicating that the last
     * update time is unknown, usually when inserted from a local file source.
     */
    public static final long UPDATED_UNKNOWN = -1;

    public interface SyncColumns {
        /** Last time this entry was updated or synchronized. */
        String UPDATED = "updated";
    }

    interface JobColumns {
        /** Unique string identifying this block of record. */
        String OBJECTID = "job_id"; //1
        String URL = "job_detail_url";
        String POST_DATE = "post_date";
        String CLOSING_DATE = "closing_date";
        String TITLE = "job_title";
        String DESCRIPTION = "job_description";
        String LAT = "latitude";
        String LONG = "longitude";
        String COMPANY_NAME = "name";
        String COMPANY_ADDRESS = "address";
        String COMPANY_LOGO_URL = "img_url";
        String CONTACT = "contact";
        String MAX_SALARY = "max";
        String MIN_SALARY = "min";
        String EXPERIENCE = "years_of_experience";
        String SOURCE = "job_source";
    }

    interface BookmarkJobColumns {
        /** Unique string identifying this block of record. */
        String OBJECTID = "job_id";
        String URL = "job_detail_url";
        String POST_DATE = "post_date";
        String CLOSING_DATE = "closing_date";
        String TITLE = "job_title";
        String DESCRIPTION = "job_description";
        String LAT = "latitude";
        String LONG = "longitude";
        String COMPANY_NAME = "name";
        String COMPANY_ADDRESS = "address";
        String COMPANY_LOGO_URL = "img_url";
        String CONTACT = "contact";
        String MAX_SALARY = "max";
        String MIN_SALARY = "min";
        String EXPERIENCE = "years_of_experience";
        String SOURCE = "job_source";
    }

    interface SearchHistoryColumns {
        /** Unique string identifying this block of record. */
        String SEARCH_TEXT = "search_text";
        String SEARCH_DATE = "search_date";

    }

    interface TimeLineColumns {
        /** Unique string identifying this block of record. */
        String TYPE = "type";
        String OBJECTID = "timeline_id";
        String CREATE_DATE = "create_date";
        String DESCRIPTION = "description";
        String IMAGE_URL = "image_url";

    }


    /**
     * Create more interface class for more tables
     */

    public static final String CONTENT_AUTHORITY = "sg.com.utrix.jobshunt";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    public static class Jobs implements BaseColumns, JobColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("jobs").build();

        /* For more info, visit http://developer.android.com/guide/topics/providers/content-provider-creating.html#MIMETypes */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.io.job"; //for multiple items notice the dir
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.io.job"; //for single item notice the item

        public static String getJobId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static class BookmarkJobs implements BaseColumns, BookmarkJobColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("bookmark").build();

        /* For more info, visit http://developer.android.com/guide/topics/providers/content-provider-creating.html#MIMETypes */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.io.bookmark"; //for multiple items notice the dir
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.io.bookmark"; //for single item notice the item


    }

    public static class SearchHistory implements BaseColumns, SearchHistoryColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("searchhistory").build();

        /* For more info, visit http://developer.android.com/guide/topics/providers/content-provider-creating.html#MIMETypes */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.io.bookmark"; //for multiple items notice the dir
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.io.bookmark"; //for single item notice the item


    }

    public static class TimeLine implements BaseColumns, TimeLineColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("timeline").build();

        /* For more info, visit http://developer.android.com/guide/topics/providers/content-provider-creating.html#MIMETypes */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.io.bookmark"; //for multiple items notice the dir
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.io.bookmark"; //for single item notice the item


    }
}
