package sg.com.utrix.jobify.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.provider.JobContract;
import sg.com.utrix.jobify.provider.JobProvider;
import sg.com.utrix.jobify.util.ImageLoader;
import sg.com.utrix.jobify.util.PrefUtils;

import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TimeLineFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TimeLineFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TimeLineFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = makeLogTag(TimeLineFragment.class);

    private static final String ARG_SECTION_NUMBER = "section_number";

    private View mRootView;
    private ListView mListView;
    //private SimpleCursorAdapter mTimeLineAdapter;
    private LoaderManager.LoaderCallbacks mCallbacks;

    private OnFragmentInteractionListener mListener;
    private ImageLoader mImageLoader;
    private TimeLineAdapter mTimeLineAdapter;


    public static TimeLineFragment newInstance(int sectionNumber) {
        TimeLineFragment fragment = new TimeLineFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    public TimeLineFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).autoShowOrHideActionBar(true);
        mCallbacks = this;

        // Inflate the layout for this fragment
        View mRootView = inflater.inflate(R.layout.fragment_time_line, container, false);
        mListView = (ListView) mRootView.findViewById(R.id.timeline_list);

        //Setup ImageLoader
        if (getActivity() instanceof ImageLoader.ImageLoaderProvider) {
            mImageLoader = ((ImageLoader.ImageLoaderProvider) getActivity()).getImageLoaderInstance();
        }

        mRootView.findViewById(R.id.create_career_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateCareerFragment registerFragment = new CreateCareerFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.container, registerFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        mTimeLineAdapter = new TimeLineAdapter(getActivity(), null, 0);
        mListView.setAdapter(mTimeLineAdapter);

        JobProvider.clearDbflag = true;
        getLoaderManager().restartLoader(0, null, mCallbacks);


        return mRootView;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri timeLineUri = JobContract.TimeLine.CONTENT_URI.buildUpon().build();

        String queryString = "?userId=" + Uri.encode(PrefUtils.getUserId(getActivity()));
        Uri uri = Uri.parse(timeLineUri + queryString);

        JobProvider.flag = true;
        return new CursorLoader(getActivity(), uri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mTimeLineAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private interface TimeLineQuery {

        int ID = 0;
        int OBJECTID = 1;
        int TYPE = 2;
        int CREATE_DATE = 3;
        int DESCRIPTION = 4;
        int IMAGE_URL = 5;
    }

    public class TimeLineAdapter extends CursorAdapter {



        public TimeLineAdapter(Context context, Cursor cursor, int flags) {
            super(context, cursor, flags);
            //mInflater = LayoutInflater.from(context);
        }

        @Override
        // elements are populated in bindView().
        public void bindView(View view, Context context, Cursor cursor) {

            ViewHolder holder = (ViewHolder)view.getTag();

            String description = cursor.getString(TimeLineQuery.DESCRIPTION);
            String imageURL = cursor.getString(TimeLineQuery.IMAGE_URL);

            holder.description.setText(description);

            if (mImageLoader != null) {
                mImageLoader.get(imageURL, holder.timeLineImage);
            }

        }


        @Override
        // view is created in newView()
        public View newView(Context context, Cursor cursor, ViewGroup parent) {

            View view = LayoutInflater.from(context).inflate(R.layout.fragment_time_line_item,  null);
            ViewHolder holder  =   new ViewHolder();

            holder.description = (TextView) view.findViewById(R.id.time_line_description_text);
            holder.timeLineImage = (ImageView) view.findViewById(R.id.time_line_image);


            view.setTag(holder);
            return view;
        }

        private class ViewHolder {
            public ImageView timeLineImage;
            public TextView description;
        }
    }

}
