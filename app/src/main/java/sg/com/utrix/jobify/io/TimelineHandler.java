package sg.com.utrix.jobify.io;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.net.Uri;

import com.turbomanage.httpclient.HttpResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

import sg.com.utrix.jobify.provider.JobContract;

import static sg.com.utrix.jobify.util.LogUtils.LOGE;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * Created by HUANG on 26/10/2014.
 */
public class TimelineHandler {

    private static final String TAG = makeLogTag(TimelineHandler.class);
    private ContentProvider mContentProvider;


    public TimelineHandler(ContentProvider contentProvider, HttpResponse response) {

        mContentProvider = contentProvider;

        String returnJSON = response.getBodyAsString();
        int statusCode = response.getStatus();

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();



        if (statusCode == 200) {
            try {

                JSONObject jobWrapper = (JSONObject) new JSONTokener(returnJSON).nextValue();
                JSONArray jobArray = jobWrapper.getJSONArray("Result");
                Uri newUri = JobContract.TimeLine.CONTENT_URI.buildUpon().appendPath("insert").build();

                for (int i = 0; i < jobArray.length(); i++) {
                    JSONObject jobObject = jobArray.getJSONObject(i);

                    ops.add(ContentProviderOperation.newInsert(newUri)
                            .withValue(JobContract.TimeLine.OBJECTID, jobObject.getString("_id"))
                            .withValue(JobContract.TimeLine.TYPE, jobObject.getString(JobContract.TimeLine.TYPE))
                            .withValue(JobContract.TimeLine.CREATE_DATE, jobObject.getString(JobContract.TimeLine.CREATE_DATE))
                            .withValue(JobContract.TimeLine.DESCRIPTION, jobObject.getString(JobContract.TimeLine.DESCRIPTION))
                            .withValue(JobContract.TimeLine.IMAGE_URL, jobObject.getString(JobContract.TimeLine.IMAGE_URL))
                            .build());
                }

                applyBatch(ops);

            } catch (JSONException e) {
                LOGE(TAG, "Failed to parse JSON.", e);
            }
        }

        /*
        if (statusCode == 404) {
            Toast.makeText(contentProvider.getContext(), "Unable to find more jobs listing",
                    Toast.LENGTH_SHORT).show();

        }*/



    }




    public void applyBatch(ArrayList<ContentProviderOperation> ops){
        try {
            mContentProvider.applyBatch(ops);
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
}
