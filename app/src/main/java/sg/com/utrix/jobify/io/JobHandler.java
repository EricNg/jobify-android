package sg.com.utrix.jobify.io;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.turbomanage.httpclient.HttpResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

import sg.com.utrix.jobify.provider.JobContract;
import sg.com.utrix.jobify.ui.ListingFragment;

import static sg.com.utrix.jobify.util.LogUtils.LOGE;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * Created by HUANG on 26/8/2014.
 */
public class JobHandler {

    private static final String TAG = makeLogTag(JobHandler.class);
    private ContentProvider mContentProvider;


    public JobHandler(ContentProvider contentProvider, HttpResponse response) {

        mContentProvider = contentProvider;

        String returnJSON = response.getBodyAsString();
        int statusCode = response.getStatus();

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();


        if (statusCode == 200) {
            try {

                JSONObject jobWrapper = (JSONObject) new JSONTokener(returnJSON).nextValue();

                JSONArray jobArray = jobWrapper.getJSONArray("Result");
                Uri newUri = JobContract.Jobs.CONTENT_URI.buildUpon().build();

                for (int i = 0; i < jobArray.length(); i++) {
                    JSONObject jobObject = jobArray.getJSONObject(i);

                    JSONObject companyObject = jobObject.getJSONObject("company");
                    JSONObject salaryObject = jobObject.getJSONObject("salary");

                    ops.add(ContentProviderOperation.newInsert(newUri)

                            .withValue(JobContract.Jobs.OBJECTID, jobObject.getString("_id"))
                            .withValue(JobContract.Jobs.POST_DATE, jobObject.getString(JobContract.Jobs.POST_DATE))
                            .withValue(JobContract.Jobs.TITLE, jobObject.getString(JobContract.Jobs.TITLE))
                            .withValue(JobContract.Jobs.DESCRIPTION, jobObject.getString(JobContract.Jobs.DESCRIPTION))
                            .withValue(JobContract.Jobs.EXPERIENCE, jobObject.getString(JobContract.Jobs.EXPERIENCE))
                            .withValue(JobContract.Jobs.URL, jobObject.getString(JobContract.Jobs.URL))

                            .withValue(JobContract.Jobs.MIN_SALARY, salaryObject.getString(JobContract.Jobs.MIN_SALARY))
                            .withValue(JobContract.Jobs.MAX_SALARY, salaryObject.getString(JobContract.Jobs.MAX_SALARY))

                            .withValue(JobContract.Jobs.SOURCE, jobObject.getString("source"))

                            .withValue(JobContract.Jobs.COMPANY_NAME,
                                    companyObject.getString(JobContract.Jobs.COMPANY_NAME).equals("null") ? "" :companyObject.getString(JobContract.Jobs.COMPANY_NAME))
                            .withValue(JobContract.Jobs.COMPANY_LOGO_URL, companyObject.getString(JobContract.Jobs.COMPANY_LOGO_URL))
                            .withValue(JobContract.Jobs.LAT, companyObject.getString(JobContract.Jobs.LAT))
                            .withValue(JobContract.Jobs.LONG, companyObject.getString(JobContract.Jobs.LONG))
                            .withValue(JobContract.Jobs.COMPANY_ADDRESS,
                                    companyObject.getString(JobContract.Jobs.COMPANY_ADDRESS).equals("null") ? "" : companyObject.getString(JobContract.Jobs.COMPANY_ADDRESS))

                            .build());

                }

                applyBatch(ops);

            } catch (JSONException e) {
                LOGE(TAG, "Failed to parse JSON.", e);

            }
        }


        if (statusCode == 404) {
           //Toast.makeText(contentProvider.getContext(), "Unable to find more jobs listing", Toast.LENGTH_SHORT).show();
            //ListingFragment.mListView.removeFooterView(ListingFragment.mLoaderView);
           ListingFragment.mLoaderView.setVisibility(View.GONE);

        }

        /*Any updates to the UI in an Android application must happen in the UI thread.
        If you spawn a thread to do work in the background you must marshal the results
        back to the UI thread before you touch a View. Use the Handler class to
        perform the marshaling.

        http://stackoverflow.com/questions/5400288/update-textview-from-thread-please-help
         */


        ListingFragment.mHandler.post(new Runnable() {
            @Override
            public void run() {
                // This gets executed on the UI thread so it can safely modify Views
                ListingFragment.mSwipeRefreshLayout.setRefreshing(false);
                //ListingFragment.mListView.removeFooterView(ListingFragment.mLoaderView);
            }
        });
    }




    public void applyBatch(ArrayList<ContentProviderOperation> ops){
        try {
            mContentProvider.applyBatch(ops);
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
}
