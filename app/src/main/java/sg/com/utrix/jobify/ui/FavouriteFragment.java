package sg.com.utrix.jobify.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardCursorAdapter;
import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.provider.JobContract;
import sg.com.utrix.jobify.provider.JobProvider;
import sg.com.utrix.jobify.util.Config;
import sg.com.utrix.jobify.util.ImageLoader;

import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;
import static sg.com.utrix.jobify.util.TimeUtils.getTimeAgo;
import static sg.com.utrix.jobify.util.TimeUtils.timestampToMillis;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavouriteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavouriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class FavouriteFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = makeLogTag(FavouriteFragment.class);

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SECTION_NUMBER = "section_number";

    private View mRootView;
    private ListView mListView;
    private ImageLoader mImageLoader;
    private BookmarkAdapter mAdapter;


    private OnFragmentInteractionListener mListener;


    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance(int sectionNumber) {
        FavouriteFragment fragment = new FavouriteFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    public FavouriteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_favourite, container, false);
        mListView = (ListView) mRootView.findViewById(R.id.favourite_list);
        if (getActivity() instanceof ImageLoader.ImageLoaderProvider) {
            mImageLoader = ((ImageLoader.ImageLoaderProvider) getActivity()).getImageLoaderInstance();
        }

        mAdapter = new BookmarkAdapter(getActivity());
        if (mListView != null) {
            mListView.setAdapter(mAdapter);
        }

        getLoaderManager().initLoader(0, null, this);
        ((HomeActivity) getActivity()).autoShowOrHideActionBar(true);

        return mRootView;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = JobContract.BookmarkJobs.CONTENT_URI.buildUpon().build();

        //Flag to indicate call is from UI
        JobProvider.flag = true;

        return new CursorLoader(getActivity(), uri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    private interface JobQuery {

        int ID = 0;
        int TITLE = 5;
        int POST_DATE = 3;
        int COMPANY_NAME = 9;
        int COMPANY_ADDRESS = 10;
        int COMPANY_LOGO_URL = 11;
        int SOURCE = 16;
    }

    private class BookmarkAdapter extends CardCursorAdapter {

        public BookmarkAdapter(Context context) {
            super(context);
        }

        @Override
        protected Card getCardFromCursor(Cursor cursor) {
            MyCursorCard cursorCard = new MyCursorCard(super.getContext());

            cursorCard.id = cursor.getString(JobQuery.ID);
            cursorCard.title = cursor.getString(JobQuery.TITLE);
            cursorCard.postDate = cursor.getString(JobQuery.POST_DATE);
            cursorCard.companyName = cursor.getString(JobQuery.COMPANY_NAME);
            cursorCard.companyAddress = cursor.getString(JobQuery.COMPANY_ADDRESS);
            cursorCard.companyLogoUrl = cursor.getString(JobQuery.COMPANY_LOGO_URL);
            cursorCard.source = cursor.getString(JobQuery.SOURCE);

            return cursorCard;

        }

        private class MyCursorCard extends Card {

            String id;
            String title;
            String postDate;
            String companyName;
            String companyAddress;
            String companyLogoUrl;
            String source;


            public MyCursorCard(Context context) {
                super(context, R.layout.fragment_listing_item);
            }

            @Override
            public void setupInnerViewElements(ViewGroup parent, View view) {

                //Simple clickListener
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Toast.makeText(getContext(), "Card  id=" + id, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), DetailActivity.class);
                        intent.putExtra(Config.JOB_ID_BUNDLE, id);
                        intent.putExtra(Config.JOB_BUNDLE_SOURCE, "favourite");
                        startActivity(intent);
                        //getActivity().overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);

                    }
                });

                /*
                ViewGroup.LayoutParams lp = view.getLayoutParams();
                lp.height = 500;
                view.setLayoutParams(lp);*/

                final ImageView imageView = (ImageView) view.findViewById(R.id.company_logo_image);

                if (mImageLoader != null) {
                    mImageLoader.get(companyLogoUrl, imageView);
                }

                TextView jobTitle = (TextView) view.findViewById(R.id.job_title_text);
                Typeface font = Typeface.createFromAsset(mContext.getResources().getAssets(), "Roboto-Thin.ttf");
                jobTitle.setTypeface(font);

                //Set all text
                jobTitle.setText(title);
                ((TextView) view.findViewById(R.id.company_name_text)).setText(companyName);
                //((TextView) view.findViewById(R.id.company_address_text)).setText(companyAddress.split("\n")[0]);

                long time = timestampToMillis(postDate, System.currentTimeMillis());
                String timeAgo = getTimeAgo(time, getContext());

                ((TextView) view.findViewById(R.id.source_text)).setText(source + " - " + timeAgo);


            }
        }
    }

}
