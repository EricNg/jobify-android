package sg.com.utrix.jobify.ui;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.HttpResponse;
import com.turbomanage.httpclient.android.AndroidHttpClient;

import org.json.JSONObject;
import org.json.JSONTokener;

import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.util.PrefUtils;
import sg.com.utrix.jobify.util.UIUtils;

import static sg.com.utrix.jobify.util.Config.BASE_URL;
import static sg.com.utrix.jobify.util.LogUtils.LOGI;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RegisterFragment extends Fragment {

    private static final String TAG = makeLogTag(RegisterFragment.class);

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SECTION_NUMBER = "section_number";

    private View mRootView;
    private ProgressBar mLoaderView;
    private EditText mEmail;
    private EditText mPassword;
    private EditText mConfirmPassword;
    private EditText mDisplayName;
    private Button mSubmit;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance(int sectionNumber) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        // Inflate the layout for this fragment
        mRootView =  inflater.inflate(R.layout.fragment_register, container, false);

        mEmail = (EditText) mRootView.findViewById(R.id.email_text);
        mPassword = (EditText) mRootView.findViewById(R.id.password_text);
        mConfirmPassword = (EditText) mRootView.findViewById(R.id.confirm_password_text);
        mDisplayName = (EditText) mRootView.findViewById(R.id.user_name_text);
        mSubmit = (Button) mRootView.findViewById(R.id.register_user_btn);
        mLoaderView = (ProgressBar) mRootView.findViewById(R.id.register_progressBar);

        setTypeFace();


        mSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (validateForm()) {
                    postUserToServer();
                }
            }

        });

        return mRootView;
    }

    private boolean validateForm() {

        boolean validate = true;
        String emailValue = mEmail.getText().toString();
        String passwordValue = mPassword.getText().toString();
        String confirmPasswordValue = mConfirmPassword.getText().toString();
        String displayNameValue = mDisplayName.getText().toString();

        if (!UIUtils.isValidEmail(emailValue)){
            mEmail.setError("A valid email address is required");
            validate = false;
        } else {
            mEmail.setError(null);
        }

        if (passwordValue.isEmpty()){
            mPassword.setError("Password is must be between 8 to 50 characters.");
            validate = false;
        } else {
            mPassword.setError(null);
        }


        if (!confirmPasswordValue.equals(passwordValue)){
            mConfirmPassword.setError("Password does not match");
            validate = false;
        }else {
            mConfirmPassword.setError(null);
        }


        if (displayNameValue.isEmpty()){
            mDisplayName.setError("Your name must be between 5 to 255 characters.");
            validate = false;
        }else {
            mDisplayName.setError(null);
        }

        return validate;

    }

    private void setTypeFace() {
        ((TextView) mRootView.findViewById(R.id.register_header_text))
                .setTypeface(UIUtils.getHeaderFontRegular(this.getActivity()));

        ((TextView) mRootView.findViewById(R.id.register_header2_text))
                .setTypeface(UIUtils.getHeaderFontLight(this.getActivity()));

        mPassword.setTypeface(Typeface.DEFAULT);
        mPassword.setTransformationMethod(new PasswordTransformationMethod());

        //Set the typeface for password field because textfield has a different one from edittext
        mConfirmPassword.setTypeface(Typeface.DEFAULT);
        mConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
    }


    private void postUserToServer() {

        mSubmit.setVisibility(View.GONE);
        mLoaderView.setVisibility(View.VISIBLE);

        AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", mEmail.getText());
            jsonObject.put("password", mPassword.getText());
            jsonObject.put("display_name", mDisplayName.getText());
        } catch (Exception e) {
            //Do nothing for now
        }

        byte[] data = jsonObject.toString().getBytes();

        httpClient.post("/users", "application/json", data, new AsyncCallback() {

            @Override
            public void onComplete(HttpResponse httpResponse) {
                mLoaderView.setVisibility(View.GONE);
                mSubmit.setVisibility(View.VISIBLE);
                switch (httpResponse.getStatus()){
                    case 200:
                        //Success!
                        insertToPref(httpResponse.getBodyAsString());
                        break;

                    case 400:
                        Toast.makeText(getActivity(), "Invalid parameters. Please try again", Toast.LENGTH_LONG).show();
                        break;

                    case 409:
                        mEmail.setError("Email address already exists. Please use another email address");
                        mEmail.setFocusable(true);
                        mEmail.setFocusableInTouchMode(true);
                        break;

                    case 500:
                        Toast.makeText(getActivity(), "Server maybe busy. Please try again.", Toast.LENGTH_LONG).show();
                        break;

                }


            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                mSubmit.setVisibility(View.VISIBLE);
                mLoaderView.setVisibility(View.GONE);
            }
        });
    }


    private void insertToPref(String returnMsg) {
        try {
            JSONObject returnWrapper = (JSONObject) new JSONTokener(returnMsg).nextValue();

            JSONObject userIdObject = returnWrapper.getJSONObject("Result");

            PrefUtils.setUserId(getActivity(), userIdObject.getString("_id"));


        } catch (Exception e) {
        }

        LOGI(TAG, "Pref ID is " + PrefUtils.getUserId(getActivity()));

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
