package sg.com.utrix.jobify.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.HttpResponse;
import com.turbomanage.httpclient.android.AndroidHttpClient;

import org.json.JSONObject;
import org.json.JSONTokener;

import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.util.PrefUtils;
import sg.com.utrix.jobify.util.UIUtils;

import static sg.com.utrix.jobify.util.Config.BASE_URL;
import static sg.com.utrix.jobify.util.LogUtils.LOGI;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class LoginFragment extends Fragment {

    private static final String TAG = makeLogTag(LoginFragment.class);

    private static final String ARG_SECTION_NUMBER = "section_number";

    private View mRootView;
    private ProgressBar mLoaderView;
    private EditText mEmail;
    private EditText mPassword;
    private Button mSubmit;
    private Button mRegister;


    private OnFragmentInteractionListener mListener;


    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance(int sectionNumber) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mRootView =  inflater.inflate(R.layout.fragment_login, container, false);

        mEmail = (EditText) mRootView.findViewById(R.id.email_text);
        mPassword = (EditText) mRootView.findViewById(R.id.password_text);
        mLoaderView = (ProgressBar) mRootView.findViewById(R.id.register_progressBar);
        mSubmit = (Button) mRootView.findViewById(R.id.login_user_btn);
        mRegister = (Button) mRootView.findViewById(R.id.register_user_btn);

        setTypeFace();
        ((HomeActivity) getActivity()).autoShowOrHideActionBar(true);

        mSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (validateForm()) {
                    postUserToServer();
                }
            }

        });

        mRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                RegisterFragment registerFragment = new RegisterFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();


                transaction.replace(R.id.container, registerFragment);
                //transaction.setCustomAnimations(R.anim.slide_up_fragment,R.anim.slide_down_fragment,R.anim.slide_up_fragment,R.anim.slide_down_fragment );
                transaction.addToBackStack("Login");
                transaction.commit();
            }

        });

        return mRootView;
    }


    private boolean validateForm() {

        boolean validate = true;
        String emailValue = mEmail.getText().toString();
        String passwordValue = mPassword.getText().toString();

        if (!UIUtils.isValidEmail(emailValue)){
            mEmail.setError("A valid email address is required");
            validate = false;
        } else {
            mEmail.setError(null);
        }

        if (passwordValue.isEmpty()){
            mPassword.setError("Password is must be between 8 to 50 characters.");
            validate = false;
        } else {
            mPassword.setError(null);
        }

        return validate;

    }

    private void setTypeFace() {
        ((TextView) mRootView.findViewById(R.id.register_header_text))
                .setTypeface(UIUtils.getHeaderFontRegular(this.getActivity()));

        ((TextView) mRootView.findViewById(R.id.register_header2_text))
                .setTypeface(UIUtils.getHeaderFontLight(this.getActivity()));

        mPassword.setTypeface(Typeface.DEFAULT);
        mPassword.setTransformationMethod(new PasswordTransformationMethod());

    }


    private void postUserToServer() {

        mSubmit.setVisibility(View.GONE);
        mLoaderView.setVisibility(View.VISIBLE);

        AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", mEmail.getText());
            jsonObject.put("password", mPassword.getText());
        } catch (Exception e) {
            //Do nothing for now
        }

        byte[] data = jsonObject.toString().getBytes();

        httpClient.post("/users/auth", "application/json", data, new AsyncCallback() {

            @Override
            public void onComplete(HttpResponse httpResponse) {
                mLoaderView.setVisibility(View.GONE);
                mSubmit.setVisibility(View.VISIBLE);
                switch (httpResponse.getStatus()){
                    case 200:
                        //Success!
                        insertToPref(httpResponse.getBodyAsString());
                        break;

                    case 400:
                        Toast.makeText(getActivity(), "Invalid parameters. Please try again", Toast.LENGTH_LONG).show();
                        break;

                    case 401:
                        mEmail.setError("Invalid email address");
                        mPassword.setError("Invalid password");
                        Toast.makeText(getActivity(), "Invalid email or password.", Toast.LENGTH_LONG).show();
                        break;

                    case 500:
                        Toast.makeText(getActivity(), "Server maybe busy. Please try again.", Toast.LENGTH_LONG).show();
                        break;

                }


            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                mSubmit.setVisibility(View.VISIBLE);
                mLoaderView.setVisibility(View.GONE);
            }
        });
    }

    private void insertToPref(String returnMsg) {
        try {
            JSONObject returnWrapper = (JSONObject) new JSONTokener(returnMsg).nextValue();

            JSONObject userIdObject = returnWrapper.getJSONObject("Result");

            PrefUtils.setUserId(getActivity(), userIdObject.getString("_id"));


        } catch (Exception e) {
        }

        LOGI(TAG, "Pref ID is " + PrefUtils.getUserId(getActivity()));

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
