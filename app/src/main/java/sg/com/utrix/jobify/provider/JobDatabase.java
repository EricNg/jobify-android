package sg.com.utrix.jobify.provider;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import sg.com.utrix.jobify.provider.JobContract.*;

import static sg.com.utrix.jobify.util.LogUtils.LOGD;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * Helper for managing {@link android.database.sqlite.SQLiteDatabase} that stores data for
 */
public class JobDatabase extends SQLiteOpenHelper{
    private static final String TAG = makeLogTag(JobDatabase.class);

    private static final String DATABASE_NAME = "jobshunt.db";
    private static final int DATABASE_VERSION = 100;


    interface Tables {
        String JOBS = "jobs";
        String BOOKMARK = "bookmark";
        String SEARCHHISTORY = "searchhistory";
        String TIMELINE = "timeline";
    }

    public JobDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.JOBS + "("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + JobColumns.OBJECTID + " TEXT NOT NULL,"  //Column 1
                + JobColumns.URL + " TEXT NULL,"
                + JobColumns.POST_DATE + " TEXT NULL,"
                + JobColumns.CLOSING_DATE + " TEXT NULL,"
                + JobColumns.TITLE + " TEXT NULL,"
                + JobColumns.DESCRIPTION + " TEXT NULL,"
                + JobColumns.LAT + " TEXT NULL,"
                + JobColumns.LONG + " TEXT NULL,"
                + JobColumns.COMPANY_NAME + " TEXT NULL,"
                + JobColumns.COMPANY_ADDRESS + " TEXT NULL,"
                + JobColumns.COMPANY_LOGO_URL + " TEXT NULL,"
                + JobColumns.CONTACT + " TEXT NULL,"
                + JobColumns.MAX_SALARY + " TEXT NULL,"
                + JobColumns.MIN_SALARY + " TEXT NULL,"
                + JobColumns.EXPERIENCE + " TEXT NULL,"
                + JobColumns.SOURCE + " TEXT NULL ,"

                + "UNIQUE (" + JobColumns.OBJECTID + ") ON CONFLICT REPLACE)");


        db.execSQL("CREATE TABLE " + Tables.BOOKMARK + "("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + JobColumns.OBJECTID + " TEXT NOT NULL,"  //Column 1
                + JobColumns.URL + " TEXT NULL,"
                + JobColumns.POST_DATE + " TEXT NULL,"
                + JobColumns.CLOSING_DATE + " TEXT NULL,"
                + JobColumns.TITLE + " TEXT NULL,"
                + JobColumns.DESCRIPTION + " TEXT NULL,"
                + JobColumns.LAT + " TEXT NULL,"
                + JobColumns.LONG + " TEXT NULL,"
                + JobColumns.COMPANY_NAME + " TEXT NULL,"
                + JobColumns.COMPANY_ADDRESS + " TEXT NULL,"
                + JobColumns.COMPANY_LOGO_URL + " TEXT NULL,"
                + JobColumns.CONTACT + " TEXT NULL,"
                + JobColumns.MAX_SALARY + " TEXT NULL,"
                + JobColumns.MIN_SALARY + " TEXT NULL,"
                + JobColumns.EXPERIENCE + " TEXT NULL,"
                + JobColumns.SOURCE + " TEXT NULL ,"

                + "UNIQUE (" + JobColumns.OBJECTID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.SEARCHHISTORY + "("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SearchHistory.SEARCH_TEXT + " TEXT NULL,"
                + SearchHistory.SEARCH_DATE + " INT NULL,"
                + "UNIQUE (" + SearchHistory.SEARCH_TEXT + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.TIMELINE + "("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TimeLine.OBJECTID + " TEXT NOT NULL,"
                + TimeLine.TYPE + " TEXT NULL,"
                + TimeLine.CREATE_DATE + " TEXT NULL,"
                + TimeLine.DESCRIPTION + " TEXT NULL,"
                + TimeLine.IMAGE_URL + " TEXT NULL,"
                + "UNIQUE (" + TimeLine.OBJECTID + ") ON CONFLICT REPLACE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LOGD(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.JOBS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TIMELINE);


    }
}
