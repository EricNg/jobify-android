package sg.com.utrix.jobify.ui;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.HttpResponse;
import com.turbomanage.httpclient.android.AndroidHttpClient;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import sg.com.utrix.jobify.R;
import sg.com.utrix.jobify.util.PrefUtils;

import static sg.com.utrix.jobify.util.Config.BASE_URL;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class CreateCareerFragment extends Fragment {

    private static final String TAG = makeLogTag(CreateCareerFragment.class);
    private View mRootView;
    private EditText mPost;
    private Button mSubmit;
    private Button mUpload;
    private Spinner mSpinner;
    private ImageView mPostImage;
    private ProgressBar mLoaderView;
    private Bitmap mBitmap;
    private String mImageId;
    int SELECT_FILE = 1;
    int REQUEST_CAMERA = 2;



    public CreateCareerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        mRootView = inflater.inflate(R.layout.fragment_create_career, container, false);

        mPost = (EditText) mRootView.findViewById(R.id.post_career);
        mUpload = (Button) mRootView.findViewById(R.id.upload_image);
        mSpinner = (Spinner) mRootView.findViewById(R.id.career_type_spinner);
        mSubmit = (Button) mRootView.findViewById(R.id.post_career_btn);
        mLoaderView = (ProgressBar) mRootView.findViewById(R.id.register_progressBar);
        mPostImage = (ImageView) mRootView.findViewById(R.id.post_image);
        populateSpinner();

        mUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        mSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (validateForm()) {
                    //postDataToServer();
                    new postImageToServer().execute();
                }
            }

        });

        // Inflate the layout for this fragment
        return mRootView;
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose an image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment
                            .getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bm;
                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();

                    mBitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            btmapOptions);

                    //bm = Bitmap.createScaledBitmap(bm, 70, 70, true);
                    mPostImage.setImageBitmap(mBitmap);

                    String path = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "jobify" + File.separator + "default";
                    f.delete();
                    OutputStream fOut = null;
                    File file = new File(path, String.valueOf(System
                            .currentTimeMillis()) + ".jpg");
                    try {
                        fOut = new FileOutputStream(file);
                        fOut.flush();
                        fOut.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();

                String tempPath = getPath(selectedImageUri, getActivity());

                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                mBitmap = BitmapFactory.decodeFile(tempPath, btmapOptions);
                mPostImage.setImageBitmap(mBitmap);
            }
        }

        //mPostImage.getLayoutParams().height = 600;
    }

    public String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private boolean validateForm() {

        boolean validate = true;
        String postValue = mPost.getText().toString();


        if (postValue.isEmpty()){
            mPost.setError("Description must be between 1 to 140 characters.");
            validate = false;
        } else {
            mPost.setError(null);
        }

        return validate;

    }

    private void postDataToServer() {

        mSubmit.setVisibility(View.GONE);
        mLoaderView.setVisibility(View.VISIBLE);

        AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token", PrefUtils.getUserId(getActivity()));
            jsonObject.put("type", mSpinner.getSelectedItem().toString());
            jsonObject.put("description", mPost.getText());
            jsonObject.put("image", mImageId);
        } catch (Exception e) {
            //Do nothing for now
        }

        byte[] data = jsonObject.toString().getBytes();

        httpClient.post("/timelines", "application/json", data, new AsyncCallback() {

            @Override
            public void onComplete(HttpResponse httpResponse) {
                mLoaderView.setVisibility(View.GONE);
                mSubmit.setVisibility(View.VISIBLE);
                switch (httpResponse.getStatus()){
                    case 200:
                        //Success!
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
                        break;

                    case 400:
                        Toast.makeText(getActivity(), "Invalid parameters. Please try again", Toast.LENGTH_LONG).show();
                        break;

                    case 409:

                        break;

                    case 500:
                        Toast.makeText(getActivity(), "Server maybe busy. Please try again.", Toast.LENGTH_LONG).show();
                        break;

                }


            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                mSubmit.setVisibility(View.VISIBLE);
                mLoaderView.setVisibility(View.GONE);
            }
        });
    }

    private class postImageToServer extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            try {

                //Set up Http
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpPost httpPost = new HttpPost(BASE_URL + "/uploads");

                //Deal with the image
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 35, bos);
                byte[] data = bos.toByteArray();

                //Set Data and Content-type header for the image
                MultipartEntity entity = new MultipartEntity();
                entity.addPart("image",new ByteArrayBody(data, "image/jpeg", "image.jpg"));
                httpPost.setEntity(entity);

                //Get Response
                org.apache.http.HttpResponse response = httpClient.execute(httpPost,localContext);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

                //Parse
                parseImageJson(reader.readLine());

                return null;
            } catch (Exception e) {
                // something went wrong. connection with the server error
            }

            return null;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(String result) {
            postDataToServer();
        }
    }

    private void parseImageJson(String response){
        try {
            JSONObject resultWrapper = (JSONObject) new JSONTokener(response).nextValue();
            mImageId = resultWrapper.getString("Result");
        } catch (Exception e)
        {

        }
    }

    private void populateSpinner() {

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.career_type_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        mSpinner.setAdapter(adapter);

    }


}
