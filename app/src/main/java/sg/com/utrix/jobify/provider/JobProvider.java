package sg.com.utrix.jobify.provider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.HttpResponse;
import com.turbomanage.httpclient.ParameterMap;
import com.turbomanage.httpclient.android.AndroidHttpClient;

import java.util.ArrayList;

import sg.com.utrix.jobify.io.JobHandler;
import sg.com.utrix.jobify.io.TimelineHandler;
import sg.com.utrix.jobify.provider.JobContract.*;
import sg.com.utrix.jobify.provider.JobDatabase.*;

import static sg.com.utrix.jobify.util.Config.BASE_URL;
import static sg.com.utrix.jobify.util.LogUtils.LOGI;
import static sg.com.utrix.jobify.util.LogUtils.makeLogTag;


public class JobProvider extends ContentProvider {
    private static final String TAG = makeLogTag(JobProvider.class);

    private JobDatabase mOpenHelper;
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    //Add here
    private static final int RETURN_ROW = 1;

    private static final int JOBS_LIST = 100;
    private static final int JOBS_ID = 102;

    private static final int BOOKMARK_INSERT = 200;
    private static final int BOOKMARK_GET= 201;
    private static final int BOOKMARK_ID= 202;

    private static final int SEARCH_INSERT = 300;
    private static final int SEARCH_GET = 301;

    private static final int TIMELINE_GET = 400;
    private static final int TIMELINE_INSERT = 401;

    private ContentProvider mContentProvider = this;
    public static boolean clearDbflag = false;
    public static boolean flag = true;

    //Add here
    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = JobContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, "jobs", JOBS_LIST);
        matcher.addURI(authority, "jobs/id/*", JOBS_ID);

        matcher.addURI(authority, "bookmark/insert", BOOKMARK_INSERT);
        matcher.addURI(authority, "bookmark/id/*", BOOKMARK_ID);
        matcher.addURI(authority, "bookmark", BOOKMARK_GET);

        matcher.addURI(authority , "searchhistory/insert", SEARCH_INSERT);
        matcher.addURI(authority , "searchhistory", SEARCH_GET);

        matcher.addURI(authority , "timeline", TIMELINE_GET);
        matcher.addURI(authority , "timeline/insert", TIMELINE_INSERT);

        return matcher;
    }


    @Override
    public boolean onCreate() {
        mOpenHelper = new JobDatabase(getContext());
        return true;
    }


    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);

        //Add here
        switch (match) {
            case JOBS_LIST:
                return Jobs.CONTENT_TYPE;
            case JOBS_ID:
                return Jobs.CONTENT_ITEM_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();

        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);

        long rowId;

        switch (match) {
            case JOBS_LIST:
                rowId = db.insertWithOnConflict(Tables.JOBS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;

            case BOOKMARK_INSERT:
                rowId = db.insertWithOnConflict(Tables.BOOKMARK, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;

            case SEARCH_INSERT:
                rowId = db.insertWithOnConflict(Tables.SEARCHHISTORY, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;

            case TIMELINE_INSERT:
                rowId = db.insertWithOnConflict(Tables.TIMELINE, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;

            default:
                throw new IllegalArgumentException("unsupported uri in insert");

        }

        //Notify the uri when a record is added
        if (rowId > 0) {
            notifyChange(uri);

            return uri;
        } else {
            return null;
        }

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        LOGI(TAG, "query(uri=" + uri + ")");
        AndroidHttpClient httpClient = new AndroidHttpClient(BASE_URL);
        httpClient.setMaxRetries(5);

        /*
         * Gets a writeable database. This will trigger its creation if it doesn't already exist.
         */
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        String queryText;
        ParameterMap params = httpClient.newParams();
        String urlParam;
        long id;

        Cursor mCursor;
        //Add here
        switch (match) {

            case JOBS_LIST:

               queryText = uri.getQueryParameter("keywords");

                urlParam = "/jobs";

                //Whenever the edittext is submitted, clear db
                if (clearDbflag) {
                    db.delete(Tables.JOBS, null, null);
                    clearDbflag = false;
                    params = params.add("keywords", queryText);
                }


                mCursor = db.query(Tables.JOBS, projection, null, null, null, null, null);
                mCursor.setNotificationUri(getContext().getContentResolver(), uri);


                if ((mCursor.getCount() > 0) && (flag)) {
                    mCursor.moveToLast();
                    params = params.add("keywords", queryText);
                    params = params.add("after", mCursor.getString(1));

                }

                if (flag) {
                    //Send request to API
                    httpClient.get(urlParam, params, new AsyncCallback() {

                        @Override
                        public void onComplete(HttpResponse httpResponse) {
                            new JobHandler(mContentProvider, httpResponse);
                        }

                        @Override
                        public void onError(Exception e) {
                            e.printStackTrace();
                        }
                    });
                }

                return mCursor;

            case JOBS_ID:

                id = ContentUris.parseId(uri);
                mCursor = db.query(Tables.JOBS, projection, "_id="+id, null, null, null, null);

                return mCursor;

            case BOOKMARK_ID:

                id = ContentUris.parseId(uri);
                mCursor = db.query(Tables.BOOKMARK, projection, "_id="+id, null, null, null, null);

                return mCursor;

            case BOOKMARK_GET:
                mCursor = db.query(Tables.BOOKMARK, projection, null, null, null, null, null);
                return  mCursor;

            case SEARCH_GET:
                mCursor = db.query(Tables.SEARCHHISTORY, projection, null, null, null, null, "_id DESC", "20");
                return mCursor;

            case TIMELINE_GET:

                queryText = uri.getQueryParameter("userId");
                urlParam = "/timelines/" + queryText;

                //Send request to API
                httpClient.get(urlParam, params, new AsyncCallback() {

                    @Override
                    public void onComplete(HttpResponse httpResponse) {
                        new TimelineHandler(mContentProvider, httpResponse);
                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                    }
                });

                mCursor = db.query(Tables.TIMELINE, projection, null, null, null, null, "_id DESC", "20");
                return mCursor;

            default:
                throw new IllegalArgumentException("unsupported uri ");
        }
    }


    private void notifyChange(Uri uri) {
        flag = false;
        Context context = getContext();
        context.getContentResolver().notifyChange(uri, null, false);


        // Widgets can't register content observers so we refresh widgets separately.
        //context.sendBroadcast(ScheduleWidgetProvider.getRefreshBroadcastIntent(context, false));
    }

}
